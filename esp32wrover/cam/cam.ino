#include <WiFi.h>
#include <WebServer.h>
#include <esp32cam.h>
#include <esp_camera.h>
#include <ESPmDNS.h>
#include <HTTPClient.h>

const char *ssid_Router     = "camwifi";
const char *password_Router = "microshift";
camera_config_t config;

#define HOSTNAME "microshift-cam-reg"
#define HOSTNAME_FULL HOSTNAME ".local"
#define PWDN_GPIO_NUM    -1
#define RESET_GPIO_NUM   -1
#define XCLK_GPIO_NUM    21
#define SIOD_GPIO_NUM    26
#define SIOC_GPIO_NUM    27
#define Y9_GPIO_NUM      35
#define Y8_GPIO_NUM      34
#define Y7_GPIO_NUM      39
#define Y6_GPIO_NUM      36
#define Y5_GPIO_NUM      19
#define Y4_GPIO_NUM      18
#define Y3_GPIO_NUM       5
#define Y2_GPIO_NUM       4
#define VSYNC_GPIO_NUM   25
#define HREF_GPIO_NUM    23
#define PCLK_GPIO_NUM    22

WebServer server(80);
IPAddress microShiftAddress;

static auto hiRes = esp32cam::Resolution::find(640, 480);
unsigned long lastReconnectMillis;
bool microShiftResolved = false;

void handleJpgHi()
{
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);

  delay(150);

  if (!esp32cam::Camera.changeResolution(hiRes)) {
    Serial.println("SET-HI-RES FAIL");
  }

  auto frame = esp32cam::capture();
  pinMode(4,OUTPUT);
  digitalWrite(4,LOW);

  if (frame == nullptr) {
    Serial.println("CAPTURE FAIL");
    server.send(503, "", "");
    return;
  }
  Serial.printf("CAPTURE OK %dx%d %db\n", frame->getWidth(), frame->getHeight(),
                static_cast<int>(frame->size()));

  server.setContentLength(frame->size());
  server.send(200, "image/jpeg");
  WiFiClient client = server.client();
  frame->writeTo(client);
}

bool anyoneStreaming;
void handleMjpeg()
{
  anyoneStreaming = true;

  if (!esp32cam::Camera.changeResolution(hiRes)) {
    Serial.println("SET-HI-RES FAIL");
  }

  Serial.println("STREAM BEGIN");
  WiFiClient client = server.client();
  auto startTime = millis();
  int res = esp32cam::Camera.streamMjpeg(client);

  anyoneStreaming = false;
  microShiftAddress = IPAddress((uint32_t)0);

  if (res <= 0) {
    Serial.printf("STREAM ERROR %d\n", res);
    return;
  }
  auto duration = millis() - startTime;
  Serial.printf("STREAM END %dfrm %0.2ffps\n", res, 1000.0 * res / duration);
}

char token[32];
char *getCamID() {
   static char cam_id[17];
   uint32_t chipId = 0;
   for(int i=0; i<17; i=i+8) {
	  chipId |= ((ESP.getEfuseMac() >> (40 - i)) & 0xff) << i;
   }
   sprintf(cam_id,"CAM_%08x", chipId);
   return cam_id;
}

void config_init();
void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();

  //WiFi.persistent(false);
  //WiFi.mode(WIFI_STA);
  WiFi.begin(ssid_Router, password_Router);

  while (WiFi.isConnected() != true) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  config_init();
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  sensor_t * s = esp_camera_sensor_get();
  s->set_brightness(s, 0);     // -2 to 2
  s->set_contrast(s, 0);       // -2 to 2
  s->set_saturation(s, 0);     // -2 to 2
  s->set_special_effect(s, 0); // 0 to 6 (0 - No Effect, 1 - Negative, 2 - Grayscale, 3 - Red Tint, 4 - Green Tint, 5 - Blue Tint, 6 - Sepia)
  s->set_whitebal(s, 1);       // 0 = disable , 1 = enable
  s->set_awb_gain(s, 1);       // 0 = disable , 1 = enable
  s->set_wb_mode(s, 0);        // 0 to 4 - if awb_gain enabled (0 - Auto, 1 - Sunny, 2 - Cloudy, 3 - Office, 4 - Home)
  s->set_exposure_ctrl(s, 1);  // 0 = disable , 1 = enable
  s->set_aec2(s, 0);           // 0 = disable , 1 = enable
  s->set_ae_level(s, 0);       // -2 to 2
  s->set_aec_value(s, 300);    // 0 to 1200
  s->set_gain_ctrl(s, 1);      // 0 = disable , 1 = enable
  s->set_agc_gain(s, 0);       // 0 to 30
  s->set_gainceiling(s, (gainceiling_t)0);  // 0 to 6
  s->set_bpc(s, 0);            // 0 = disable , 1 = enable
  s->set_wpc(s, 1);            // 0 = disable , 1 = enable
  s->set_raw_gma(s, 1);        // 0 = disable , 1 = enable
  s->set_lenc(s, 1);           // 0 = disable , 1 = enable
  s->set_hmirror(s, 0);        // 0 = disable , 1 = enable
  s->set_vflip(s, 0);          // 0 = disable , 1 = enable
  s->set_dcw(s, 1);            // 0 = disable , 1 = enable
  s->set_colorbar(s, 0);       // 0 = disable , 1 = enable

  char *cam_id = getCamID();
  if(!MDNS.begin(cam_id)) {
     Serial.println("Error starting mDNS");
     return;
  }
  Serial.print("Started mDNS server as ");
  Serial.println( HOSTNAME_FULL );

  lastReconnectMillis = millis(); 
  Serial.print("http://"); Serial.println(WiFi.localIP()); Serial.println("  /cam-hi.jpg");
  Serial.print("http://"); Serial.println(WiFi.localIP()); Serial.println("  /stream");

  server.on("/cam-hi.jpg", handleJpgHi);
  server.on("/stream", handleMjpeg);
  server.begin();
}

void reconnectWiFiOnDisconnect() {
  unsigned long currentMillis = millis();
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - lastReconnectMillis >= 20000)) {
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    lastReconnectMillis = currentMillis;
  }
}

bool registerInCamServer() {
  String requestPath = "/register?ip=";
  requestPath += WiFi.localIP().toString();
  requestPath += "&token=";
  requestPath += token;

  WiFiClient client;

  if (!client.connect(microShiftAddress, 80)) {
    Serial.println("Connection failed");
  }

  client.println("GET " + requestPath + " HTTP/1.1");
  client.println("Host: " HOSTNAME_FULL);
  client.println("Connection: close");
  client.println();
  uint8_t buf[1024];
  delay(1000);
  int l = client.read(buf, 1023);
  buf[l] = '\0';
  Serial.println((char*)buf);
  client.stop();
  return true;
}

unsigned long lastRegistrationMillis = 0;
#define RE_REGISTRATION_MS 15000
void loop() {
  reconnectWiFiOnDisconnect();

  if (WiFi.status() != WL_CONNECTED) {
    delay(100);
    return;
  }

  server.handleClient();

  if ((!microShiftAddress) || (!anyoneStreaming && (millis()-lastRegistrationMillis)>RE_REGISTRATION_MS)) {
  	microShiftAddress = MDNS.queryHost( HOSTNAME );
        if (microShiftAddress) {
          Serial.print("microshift " HOSTNAME_FULL " resolved to: ");
          Serial.println(microShiftAddress.toString());
          Serial.println("Registering to cam server");
          if (!registerInCamServer()) {
            microShiftAddress = IPAddress((uint32_t)0);
          } else {
            lastRegistrationMillis = millis();
          }
             
        } else {
          Serial.println("Could not resolve microshift address");
          delay(1000);
        }
  }
}

void config_init() {
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.frame_size = FRAMESIZE_QVGA;
  config.pixel_format = PIXFORMAT_JPEG; // for streaming
  //config.pixel_format = PIXFORMAT_RGB565; // for face detection/recognition
  config.grab_mode = CAMERA_GRAB_WHEN_EMPTY;
  config.fb_location = CAMERA_FB_IN_PSRAM;
  config.jpeg_quality = 12;
  config.fb_count = 1;
}